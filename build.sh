#!/bin/bash
set -e
rm -rf dist
mkdir -p dist
#   rm src/server/static/js/oikia4u.js
#   rm src/server/static/css/oikia4u.css

GOPATH=$(pwd):$(pwd)/vendor CGO_ENABLED=0 GOOS=linux go build -tags "dist" -ldflags "-s" -a -installsuffix cgo -o dist/mocrm server
cp -r src/server/static dist/static
cp -r src/server/views dist/views
cp src/server/*.pem dist/
cp src/server/BaltimoreCyberTrustRoot.crt.pem dist

cp src/server/config.yaml dist/
# cp -r src/server/.well-known dist/.well-known
#  gulp min-css  min-js 



